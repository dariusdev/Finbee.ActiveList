﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Documents;
using static System.Int32;
using static System.String;

namespace Finbee.Helper
{

    public class LoanItem
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string LoanSize { get; set; }
        public string Period { get; set; }
        public string Grade { get; set; }
        public string OfferedRate { get; set; }
        public string AuctionEndTime { get; set; }
        public string Link { get; set; }
        public bool InProgress { get; set; }
        public string YourPosition { get; set; }
    }


    public partial class MainWindow : Window
    {
        public static System.Timers.Timer _timer;

        public MainWindow()
        {

            InitializeTimer(1);

            InitializeComponent();
            Refresh();

            System.Windows.Forms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();
            ni.Icon = new System.Drawing.Icon("bee.ico");
            ni.Visible = true;
            ni.DoubleClick +=
                delegate (object sender, EventArgs args)
                {
                    this.Show();
                    this.WindowState = WindowState.Normal;
                };
        }

        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Minimized)
                this.Hide();

            base.OnStateChanged(e);
        }

        private void InitializeTimer(int min)
        {
            _timer = new System.Timers.Timer(min *60 * 1000);
            _timer.Elapsed += OnTimedEvent;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        private List<LoanItem> GetLoanItems()
        {
            var doc = WebClient.GetHtml();

            var items = ListExtractor.GetLoanItems(doc, txtName.Text);

            var filtered = items.Where(x => x.InProgress).OrderBy(x => x.AuctionEndTime).ToList();

            if (!String.IsNullOrEmpty(txtName.Text) && txtName.Text != "YourName")
            {
                foreach (var item in filtered)
                {
                    item.YourPosition = LoansTableLoader.GetLoansOffers(txtName.Text, item.Id)?.Status;
                }
            }

            return filtered;
        }

        private void SetDateLabel()
        {
            lblDate.Content = "Last updated: " + DateTime.Now;
            lblDate.UpdateLayout();
        }



        private void Refresh()
        {
            var loanItems = GetLoanItems();

            dataGrid.ItemsSource = loanItems;
            dataGrid.UpdateLayout();

            SetDateLabel();
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                Refresh();
            });
        }



        #region Actions
        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        private void DG_Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = (Hyperlink)e.OriginalSource;
            Process.Start(link.NavigateUri.AbsoluteUri);
        }

        private void cmbRefresh_Copy_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string content = ((System.Windows.Controls.ContentControl)(e.AddedItems[0])).Content?.ToString();

            if (!IsNullOrEmpty(content))
            {
                _timer.Stop();
                _timer.Dispose();

                var intValue = Parse(content);

                InitializeTimer(intValue);
            }

        }
        #endregion

    }
}
