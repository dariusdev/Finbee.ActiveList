﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace Finbee.Helper
{
    public static class ListExtractor
    {
        public static List<LoanItem> GetLoanItems(HtmlDocument doc, string name)
        {
            var result = new List<LoanItem>();

            var table = doc.DocumentNode.SelectSingleNode("//div[@id='loans_table']");

            var loans = table.ChildNodes.Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("loan"));
            
            foreach (HtmlNode loan in loans)
            {
                var loanItem = new LoanItem();
                loanItem.Id = GetId(loan);
                loanItem.AuctionEndTime = GetAuctionEndTime(loan);
                loanItem.Username = GetName(loan);
                loanItem.Link = GetLink(loan);
                loanItem.LoanSize = GetLoanSize(loan);
                loanItem.Period = GetPeriod(loan);
                loanItem.Grade = GetGrade(loan);
                loanItem.OfferedRate = GetOfferedRate(loan);
                loanItem.InProgress = GetStatus(loan);
                result.Add(loanItem);
            }

            return result;
        }



        private static string GetOfferedRate(HtmlNode loan)
        {
            HtmlNode node = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("attr offered_rate"));

            var userName = node.SelectNodes("./text()")[0].InnerText;

            return userName;
        }

        private static string GetName(HtmlNode loan)
        {
            HtmlNode node = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("attr user"));

            var userName = node.SelectNodes("./text()")[0].InnerText;

            return userName;
        }

        private static string GetId(HtmlNode loan)
        {
            HtmlNode node = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("more_content"));

            var value = node.Attributes["id"].Value;

            return value;
        }

        private static string GetLink(HtmlNode loan)
        {
            HtmlNode node = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("attr progress"));

            var value = node.SelectNodes("a")[0].Attributes["href"].Value;

            return value;
        }

        private static bool GetStatus(HtmlNode loan)
        {
            HtmlNode node = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("attr progress"));

            var value = node.SelectNodes("a")[0].Attributes["class"].Value;

            if (value == "button yellow" || value == "button bar")
            {
                return true;
            }

            return false;
        }

        private static string GetLoanSize(HtmlNode loan)
        {
            HtmlNode node = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("amount"));

            var value = node.SelectNodes("./text()")[0].InnerText;

            return value.Replace("&euro;","");
        }

        private static string GetPeriod(HtmlNode loan)
        {
            HtmlNode node = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("term"));

            var value = node.SelectNodes("./text()")[0].InnerText;

            return value;
        }

        private static string GetGrade(HtmlNode loan)
        {
            HtmlNode node = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("risk_cat"));

            var value = node.LastChild.InnerText;

            return value.ToUpper();
        }

        private static string GetAuctionEndTime(HtmlNode loan)
        {
            HtmlNode node = loan.Descendants("div").First(d => d.Attributes.Contains("id") && d.Attributes["id"].Value == "loan_request_details");
           

            var right = node.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value == "right");

            //Take 3 element <div class="value">2016-09-28 16:40:10</div>

            var dateValue = right.ChildNodes[2].LastChild.InnerText;


            var endDate = Convert.ToDateTime(dateValue);

            TimeSpan span = endDate.Subtract(DateTime.Now);

            //  dateValue = span.ToString(@"d\ hh\:mm");

            string days = span.Days < 10 ? $"{span.Days} ": span.Days.ToString();

            dateValue = $"{days}d  {span.Hours}h {span.Minutes}m";

            return dateValue;
        }
       
    }
}
