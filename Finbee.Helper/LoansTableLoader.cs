﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using RestSharp;

namespace Finbee.Helper
{
    public class LoanStatus
    {
        public string Id { get; set; }
        public string Status { get; set; } // 1 OK, 2 Half, 3 none
    }

    public class Investment
    {
        public string amount { get; set; }
        public string rate { get; set; }
        public string date { get; set; }
        public string state { get; set; }
        public string username { get; set; }
    }

    public static class LoansTableLoader
    {
        //private string loansUrl = "https://p2p.finbee.lt/loans_table-bids.php?id=";
        private static RestClient client = new RestClient("https://p2p.finbee.lt/loans_table-bids.php");

        public static LoanStatus GetLoansOffers(string name, string id)
        {
            var request = new RestRequest("", Method.GET);

            request.AddParameter("id", id); // adds to POST or URL querystring based on Method

            var response = client.Execute(request);

            var des = JsonConvert.DeserializeObject<List<Investment>>(response.Content);

            LoanStatus result = null;

            var index = 0;
            var endPosition = 0;
            var yourPosition = 0;
            string yourState = null;

            foreach (var investment in des)
            {
                index = index + 1;

                if (investment.username.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (string.IsNullOrEmpty(yourState))
                    {
                        yourPosition = index;
                        yourState = investment.state;
                    }
                }

                if (investment.state == "3")
                {
                    endPosition = index;
                    break;
                }
            }

            if (yourPosition != 0)
            {
                result = new LoanStatus();
                result.Id = id;
                result.Status = MapState(yourState, yourPosition, endPosition);
            }

            return result;
        }

        private static string MapState(string state, int yourPosition, int endPosition)
        {
            switch (state)
            {
                case "1":
                    return $"OK ({yourPosition} / {endPosition}) {GetDiff(yourPosition, endPosition)}";
                case "2":
                    return $"Part ({yourPosition} / {endPosition})";
                case "3":
                    return $"Lost";
            }
            return "";
        }

        public static string GetDiff(int yourPosition, int endPosition)
        {
            if (endPosition - yourPosition > 0)
            {
                return "Dif: " + (endPosition - yourPosition);
            }

            return String.Empty;
        }
    }
}
