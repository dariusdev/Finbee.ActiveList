﻿using HtmlAgilityPack;

namespace Finbee.Helper
{
    static class WebClient
    {
        private const string Url = "https://www.finbee.lt/perziureti-paskolas/";
        private static System.Net.WebClient Htmlclient = new System.Net.WebClient();

        public static HtmlDocument GetHtml()
        {
            Htmlclient.Encoding = System.Text.Encoding.UTF8;

            HtmlDocument doc = new HtmlDocument();

            string htmlCode = Htmlclient.DownloadString(Url);

            doc.LoadHtml(htmlCode);

            return doc;
        }
    }
}
