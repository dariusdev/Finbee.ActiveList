﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using HtmlAgilityPack;

namespace IntegrationTests
{
    public class LoanItem
    {
        public string Id { get; set; }
        public string Link { get; set; }
        public string Username { get; set; }
        public string LoanSize { get; set; }
        public string Period { get; set; }
        public string Grade { get; set; }
        public string AuctionEndTime { get; set; }
    }

    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void Happy_FlowTest()
        {
            var url = "https://www.finbee.lt/perziureti-paskolas/";
            var allLoans = new List<LoanItem>();

            using (WebClient client = new WebClient()) // WebClient class inherits IDisposable
            {
                client.Encoding = System.Text.Encoding.UTF8;

                string htmlCode = client.DownloadString(url);


                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(htmlCode);

                var table = doc.DocumentNode.SelectSingleNode("//div[@id='loans_table']");

                var loans = table.ChildNodes.Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("loan"));


                foreach (HtmlNode loan in loans)
                {
                    var loanItem = new LoanItem();
                    loanItem.Username = GetName(loan);
                    loan.Id = "15";
                }
            }
        }

        private string GetName(HtmlNode loan)
        {
            HtmlNode user = loan.ChildNodes.First(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("attr user"));

            var userName = user.SelectNodes("./text()")[0].InnerText;

            return userName;
        }
    }
}
