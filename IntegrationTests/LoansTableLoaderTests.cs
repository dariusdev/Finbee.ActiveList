﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Finbee.Helper;

namespace IntegrationTests
{
    public class LoanTable
    {
        public string Id { get; set; }
        public string Link { get; set; }
        public string Username { get; set; }
        public string LoanSize { get; set; }
        public string Period { get; set; }
        public string Grade { get; set; }
        public string AuctionEndTime { get; set; }
    }

    [TestClass]
    public class LoansTableLoaderTests
    {

        [TestMethod]
        public void Happy_FlowTest()
        {
            LoansTableLoader.GetLoansOffers("dariusd", "3630");
        }

  
    }
}
